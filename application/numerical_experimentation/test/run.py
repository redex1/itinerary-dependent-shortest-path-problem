from application.numerical_experimentation.experiment.utility.experimental_file_generator import \
    ExperimentalFileGenerator
from application.business_logic.algorithm.utility.heuristic import calculate_distance_time_to_goal
from pandas import read_csv
if __name__ == '__main__':
    requests = '../experiment/request_simulation/1000_request_batch.txt'
    flightspath = '../api_simulation/flights_case_one.txt'
    newflightspath = '../api_simulation/1000_blocked_flights.txt'
    branchpath = '../database_simulation/branchs_00_percent_off.txt'
    new_branch_path = '../database_simulation/branchs_{}_percent_off.txt'

    time = calculate_distance_time_to_goal(4.6097102, -74.081749, 41.3275459, 19.8186982)
    print(time)
