from pandas import read_csv
from application.business_logic.algorithm.utility.constants import UNIFORM_COST_ALGORITHM, A_STAR_ALGORITHM, \
    VORACIOUS_ALGORITHM, RESULT_FILE_PATH
from application.business_logic.algorithm.utility.network_creator import create_network
from application.business_logic.algorithm.a_star_search import a_star_search
from application.business_logic.algorithm.uniform_cost_search import uniform_cost_search
from application.business_logic.algorithm.voracious_search import voracious_search
from application.numerical_experimentation.experiment.executor import execute
from application.numerical_experimentation.experiment.utility.database_comunication import get_granchs_from_database
from application.numerical_experimentation.experiment.utility.file_extractor import get_files_names


def batch_testing(requests_file_path, flights_file_path, branchs_file_path, algorithm_name,
                  result_file_path=RESULT_FILE_PATH):
    requests = read_csv(requests_file_path, ',')
    # call DATABASE to get all the branches
    branches = get_granchs_from_database(branchs_file_path)
    # network is created here only for testing, in real situations is created each time the algorithm is run
    # and wont be necessary to send branches to execute because we will use the database table instead
    # to update the branches states !!!
    network = create_network(flights_file_path, branches)
    # only for results
    naming = get_files_names(requests_file_path, flights_file_path, branchs_file_path, algorithm_name)
    if algorithm_name == UNIFORM_COST_ALGORITHM:
        execute(uniform_cost_search, requests, network, branches, result_file_path, naming)
    elif algorithm_name == A_STAR_ALGORITHM:
        execute(a_star_search, requests, network, branches, result_file_path, naming)
    elif algorithm_name == VORACIOUS_ALGORITHM:
        execute(voracious_search, requests, network, branches, result_file_path, naming)
