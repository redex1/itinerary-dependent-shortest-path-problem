from application.business_logic.algorithm.structure.problem import Problem
from application.business_logic.algorithm.structure.rute_presenter import write_results


def execute(algorithm, requests, network, branches, result_file_path, naming):
    goal_nodes = []
    fail_nodes = []
    for i_request in range(len(requests.startPoint)):
        from_point = str(requests.startPoint.iloc[i_request])
        to_point = str(requests.endPoint.iloc[i_request])
        problem = Problem(from_point,
                          to_point,
                          network,
                          branches)
        goal_node = algorithm(problem)
        # HERE: if we want to consider capacity bound, here we have to update 'branches' states
        if goal_node is not None:
            goal_nodes.append(goal_node)
        else:
            fail_nodes.append({"from": from_point,
                               "is_active_from": branches[from_point].is_active_branch(),
                               "to": to_point,
                               "is_active_to": branches[to_point].is_active_branch()})
    write_results(result_file_path, naming, goal_nodes, fail_nodes)
    # for goal_node in goal_nodes: print(get_pretty_route(goal_node))
    # for fail_node in fail_nodes: print(get_fail_route_reason(fail_node))
