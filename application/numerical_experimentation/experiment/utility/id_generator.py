from uuid import uuid4
from pandas import read_csv


class StateGenerator:

    @staticmethod
    def create_uuid(lenght=10):
        return str(uuid4().hex[:lenght]).upper()

    @staticmethod
    def create_flights_csv(filepath):
        flights = read_csv(filepath, '-')
        flights.insert(0, "idFlight", [StateGenerator.create_uuid() for i in range(len(flights.startPoint))])
        flights.to_csv(filepath, index=False)
