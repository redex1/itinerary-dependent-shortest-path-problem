from pandas import read_csv
from application.business_logic.algorithm.structure.branch import Branch


def get_granchs_from_database(branchs_file_path):
    branches = read_csv(branchs_file_path, ',')
    branches_map = {}

    for i_branch in range(len(branches.friendly_id)):
        friendly_id = str(branches.friendly_id.iloc[i_branch])
        capacity = int(branches.capacity.iloc[i_branch])
        quantity = int(branches.quantity.iloc[i_branch])
        is_active = int(branches.is_active.iloc[i_branch])
        continent = str(branches.continent.iloc[i_branch])
        latitude = float(branches.latitude.iloc[i_branch])
        longitude = float(branches.longitude.iloc[i_branch])

        branch = Branch(friendly_id, capacity, quantity, is_active, continent, latitude, longitude)
        branches_map[str(branches.friendly_id.iloc[i_branch])] = branch

    return branches_map
