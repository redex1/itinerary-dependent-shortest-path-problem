import ntpath

from application.business_logic.algorithm.utility.constants import UNIFORM_COST_ALGORITHM, A_STAR_ALGORITHM, \
    VORACIOUS_ALGORITHM
from application.numerical_experimentation.experiment.utility.constant import REQUEST, BRANCH, FLIGHT, ALGORITHM_NAME


def get_files_names(requests_file_path, flights_file_path, branchs_file_path, algorithm_name):
    files_names = {}
    head_req, tail_req = ntpath.split(requests_file_path)
    head_fli, tail_fli = ntpath.split(flights_file_path)
    head_bra, tail_bra = ntpath.split(branchs_file_path)
    algorithm = ""
    if algorithm_name == UNIFORM_COST_ALGORITHM:
        algorithm = "UC"
    elif algorithm_name == A_STAR_ALGORITHM:
        algorithm = "AS"
    elif algorithm_name == VORACIOUS_ALGORITHM:
        algorithm = "VO"
    files_names[REQUEST] = tail_req.split('_')[0]
    files_names[FLIGHT] = tail_fli.split('_')[0]
    files_names[BRANCH] = tail_bra.split('_')[1]
    files_names[ALGORITHM_NAME] = algorithm
    return files_names
