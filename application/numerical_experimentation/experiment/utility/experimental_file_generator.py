from pandas import read_csv, DataFrame
import random


class ExperimentalFileGenerator:
    @staticmethod
    def create_empty_branch_table(base_file_path, new_file_path):
        branchs = read_csv(base_file_path)
        branchs.insert(2, "quantity", [str(0) for _ in range(len(branchs.id_branch))])
        branchs.insert(3, "is_active", [str(1) for _ in range(len(branchs.id_branch))])
        branchs.to_csv(new_file_path, index=False)

    @staticmethod
    def create_inactive_branch_table(base_file_path, new_file_path, number_inactives=10):
        branchs = read_csv(base_file_path)
        friendly_ids = set()
        while len(friendly_ids) < number_inactives:
            friendly_ids.add(random.randint(1, len(branchs.friendly_id)))
        for index in friendly_ids:
            branchs.at[index, 'is_active'] = str(0)
        branchs.to_csv(new_file_path, index=False)

    @staticmethod
    def create_empty_flights_api_response(base_file_path, new_file_path, branchs_file_path):
        flights = read_csv(base_file_path)
        branchs = read_csv(branchs_file_path, index_col="id_branch")
        capacities = []
        for flight_i in range(len(flights)):
            if branchs.loc[flights.endPoint.iloc[flight_i]].continent == \
                    branchs.loc[flights.startPoint.iloc[flight_i]].continent:
                capacities.append(random.randrange(200, 300, 10))
            else:
                capacities.append(random.randrange(250, 400, 10))
        flights.insert(5, "capacity", capacities)
        flights.insert(6, "is_active", [str(1) for _ in range(len(flights.idFlight))])
        flights.to_csv(new_file_path, index=False)

    @staticmethod
    def create_inactive_flights_api_response(base_file_path, new_file_path, number_inactives=100):
        flights = read_csv(base_file_path)
        id_flights = set()
        while len(id_flights) < number_inactives:
            id_flights.add(random.randint(1, len(flights.idFlight)))
        for index in id_flights:
            flights.at[index, 'is_active'] = str(0)
        flights.to_csv(new_file_path, index=False)

    @staticmethod
    def create_requests(num_requests, branchs_file_path, new_file_path):
        branchs = read_csv(branchs_file_path)
        requests_indexes = []
        while len(requests_indexes) < num_requests:
            requests_indexes.append(tuple(random.sample(range(1, len(branchs.id_branch)), 2)))
        requests_data = {"startPoint": [], "endPoint": []}
        for start_i, end_i in requests_indexes:
            requests_data["startPoint"].append(branchs.id_branch.iloc[start_i])
            requests_data["endPoint"].append(branchs.id_branch.iloc[end_i])
        request_dataframe = DataFrame(requests_data, columns=["startPoint", "endPoint"])
        request_dataframe.to_csv(new_file_path, index=False)

    @staticmethod
    def create_empty_flights(base_file_path, new_file_path):
        flights = read_csv(base_file_path)
        flights.insert(6, "quantity", [0] * len(flights))
        flights.to_csv(new_file_path, index=False)

    @staticmethod
    def create_blocked_flights(requests_file_path, flights_file_path, new_flights_file_path):
        requests = read_csv(requests_file_path)
        flights = read_csv(flights_file_path)
        for indexReq, rowReq in requests.iterrows():
            for indexFli, rowFli in flights.iterrows():
                if str(rowReq["startPoint"]) == str(rowFli["startPoint"]) and \
                        str(rowReq["endPoint"]) == str(rowFli["endPoint"]):
                    flights.at[indexFli, "is_active"] = 0
        flights.to_csv(new_flights_file_path, index=False)

    @staticmethod
    def create_blocked_branchs(percentage, base_file_path, new_file_path):
        branchs = read_csv(base_file_path)
        friendly_ids = set()
        number_inactives = int(len(branchs.friendly_id) * percentage / 100)
        while len(friendly_ids) < number_inactives:
            friendly_ids.add(random.randint(1, len(branchs.friendly_id)))
        for index in friendly_ids:
            branchs.at[index, 'is_active'] = str(0)
        branchs.to_csv(new_file_path, index=False)
