from application.business_logic.algorithm.utility.constants import DAY_SECONDS


class Time:
    def __init__(self, hours, minutes, seconds=0):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def time_to_seconds(self):
        return self.hours * 60 * 60 + self.minutes * 60

    def to_string(self):
        return str(self.hours).zfill(2) + ':' + str(self.minutes).zfill(2)

    @staticmethod
    def to_time(time_str):
        return Time(int(time_str[:2]), int(time_str[3:]))

    @staticmethod
    def extract_hours(time_str):
        return int(time_str[:2])

    @staticmethod
    def extract_minutes(time_str):
        return int(time_str[3:])

    @staticmethod
    def seconds_to_time(seconds):
        hours = int(seconds / (60 * 60)) % 24
        minutes = int(seconds / 60) % 60
        return Time(hours, minutes)

    @staticmethod
    def no_limit_seconds_to_time(seconds):
        hours = int(seconds / (60 * 60))
        minutes = int(seconds / 60) % 60
        return Time(hours, minutes)

    def subtract(self, other):
        result = self.time_to_seconds() - other.time_to_seconds()
        if result < 0:
            return self.seconds_to_time(result + DAY_SECONDS)
        return self.seconds_to_time(result)

    def add(self, other):
        result = self.time_to_seconds() + other.time_to_seconds()
        return self.seconds_to_time(result)

    def no_limit_add(self, other):
        result = self.time_to_seconds() + other.time_to_seconds()
        return self.no_limit_seconds_to_time(result)

    def __lt__(self, other):
        return self.time_to_seconds() < other.time_to_seconds()
