import bisect


class Frontier:
    def __init__(self, eval_function):
        self.priority_list = []
        self.eval_function = eval_function

    def add(self, node):
        bisect.insort(self.priority_list, (self.eval_function(node), node))

    def pop(self):
        (value, node) = self.priority_list.pop(0)
        return node

    def replace(self, node):
        for (i, (value, old_node)) in enumerate(self.priority_list):
            if old_node.state == node.state and old_node.action == node.action:
                self.priority_list[i] = (self.eval_function(node), node)
                return

    def __contains__(self, node):
        return any(item[1].state == node.state and item[1].action == node.action
                   for item in self.priority_list)

    def get_value(self, node):
        for value, nodei in self.priority_list:
            if nodei.state == node.state and nodei.action == node.action:
                return value

    def __len__(self):
        return len(self.priority_list)
