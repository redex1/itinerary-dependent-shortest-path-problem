from pandas import read_csv
from application.business_logic.algorithm.structure.network import Network
from application.business_logic.algorithm.utility.time import Time


def create_network(edges_filepath, branchs):
    # call API to get all the flights
    flights = read_csv(edges_filepath, ',')

    neighbors_net = {}
    for i_flight in range(len(flights.startPoint)):
        neighbors_net[str(flights.startPoint.iloc[i_flight])] = []

    for i_flight in range(len(flights.startPoint)):
        # only add ACTIVE neighbor with ACTIVE flight
        if branchs[str(flights.endPoint.iloc[i_flight])].is_active_branch() and bool(flights.is_active.iloc[i_flight]):
            neighbor = (str(flights.idFlight.iloc[i_flight]),
                        str(flights.endPoint.iloc[i_flight]),
                        str(flights.startTime.iloc[i_flight]),
                        str(flights.endTime.iloc[i_flight]),
                        Time.to_time(str(flights.endTime.iloc[i_flight])). \
                        subtract(Time.to_time(str(flights.startTime.iloc[i_flight]))).to_string())
            neighbors_net[str(flights.startPoint.iloc[i_flight])].append(neighbor)
    return Network(neighbors_net)
