from math import sin, cos, sqrt, atan2, radians
from application.business_logic.algorithm.utility.constants import EARTH_RADIUS, AVG_CRUISE_VELOCITY
from application.business_logic.algorithm.utility.time import Time


def calculate_distance_time_to_goal(latitude_from, longitude_from, latitude_to, longitude_to):
    """
    Formula del semiverseno: es una importante ecuación para la navegación astronómica,
    en cuanto al cálculo de la distancia de círculo máximo entre dos puntos de un globo
    sabiendo su longitud y su latitud.

    AVG_CRUISE_VELOCITY:
    :param latitude_from:
    :param longitude_from:
    :param latitude_to:
    :param longitude_to:
    :return:
    """
    lat1, lon1, lat2, lon2 = map(radians, [latitude_from, longitude_from, latitude_to, longitude_to])
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance_km = EARTH_RADIUS * c
    seconds = Time(distance_km / AVG_CRUISE_VELOCITY, 0).time_to_seconds()
    return Time.seconds_to_time(seconds)
