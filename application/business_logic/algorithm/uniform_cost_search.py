from application.business_logic.algorithm.brain.best_first_graph_search import best_first_graph_search


def uniform_cost_search(problem):
    """
    :param problem:
    :return: Node
    """
    eval_function = lambda node: node.transport_cost
    return best_first_graph_search(problem, eval_function)

