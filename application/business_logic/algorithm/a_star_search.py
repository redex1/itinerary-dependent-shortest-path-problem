from application.business_logic.algorithm.brain.best_first_graph_search import best_first_graph_search
from application.business_logic.algorithm.utility.time import Time


def a_star_search(problem):
    """
    :param problem:
    :return:
    """
    eval_function = lambda node: node.transport_cost.no_limit_add(problem.heuristic(node.state))

    return best_first_graph_search(problem, eval_function)
