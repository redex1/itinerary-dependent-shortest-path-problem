from application.business_logic.algorithm.structure.node import Node
from application.business_logic.algorithm.utility.frontier import Frontier


def best_first_graph_search(problem, eval_function):
    frontier = Frontier(eval_function)
    frontier.add(Node(problem.initial))
    explored = set()
    while frontier:
        node = frontier.pop()
        explored.add((node.state, node.action))
        if problem.goal_test(node.state):
            return node
        for action in problem.actions(node.state):
            child = node.child_node(problem, action)
            # this if statement is only in the case we update capacities of BRANCHS and FLIGHTS (to do: add flights
            # logic)
            # if problem.branchs[child.state].is_active_branch():
            if (child.state, child.action) not in explored and child not in frontier:
                frontier.add(child)
            elif child in frontier:
                if eval_function(child) < frontier.get_value(child):
                    frontier.replace(child)

    return None
