from application.business_logic.algorithm.brain.best_first_graph_search import best_first_graph_search


def voracious_search(problem):
    """
    :param problem:
    :return: Node
    """
    eval_function = lambda node: problem.heuristic(node.state)
    return best_first_graph_search(problem, eval_function)

