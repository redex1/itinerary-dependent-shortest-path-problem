from application.numerical_experimentation.experiment.utility.constant import REQUEST, RESULT, BRANCH, FLIGHT, \
    ALGORITHM_NAME


def get_pretty_route(goal_node):
    rute = goal_node.solution()
    separator = " | "

    start = "Desde: {}".format(rute[0][0]) + separator
    end = "Hasta: {}".format(rute[len(rute) - 1][0]) + separator
    time_spend = "Tiempo: {} Hrs".format(goal_node.transport_cost.to_string()) + separator
    pretty_route = "Ruta: "
    for i, (state, action) in enumerate(rute):
        if i == 0:
            pretty_route = pretty_route + "{} ".format(state)
        else:
            pretty_route = pretty_route + "<{}> {} ".format(action, state)

    output = start + end + time_spend + pretty_route
    return output


def get_fail_route_reason(fail_node):
    separator = " | "

    start = "Desde: {}".format(fail_node["from"]) + separator
    end = "Hasta: {}".format(fail_node["to"]) + separator
    reason = "Fallo debido a: "
    if not fail_node["is_active_from"] or not fail_node["is_active_to"]:
        reason = reason + "BRANCH_INACTIVO"
    else:
        reason = reason + "NO_EXISTE_RUTA"
    output = start + end + reason
    return output


def get_only_transport_time(goal_node):
    separator = '\n'
    time_spend = "{}".format(goal_node.transport_cost.to_string()) + separator
    return time_spend


def get_only_fail_reason(fail_node):
    separator = '\n'
    if not fail_node["is_active_from"] or not fail_node["is_active_to"]:
        reason = "BRANCH_INACTIVO" + separator
    else:
        reason = "NO_EXISTE_RUTA" + separator
    return reason


def write_results(result_file_path, naming, goal_nodes, fail_nodes):
    root_path = result_file_path + '{}_{}_{}_{}_{}_{}_{}_{}.txt'.format(RESULT, naming[ALGORITHM_NAME],
                                                                        naming[REQUEST], REQUEST,
                                                                        naming[BRANCH], BRANCH,
                                                                        naming[FLIGHT], FLIGHT)
    with open(root_path, "w+") as result_file:
        for goal_node in goal_nodes: result_file.write(get_only_transport_time(goal_node))
        for fail_node in fail_nodes: result_file.write(get_only_fail_reason(fail_node))
