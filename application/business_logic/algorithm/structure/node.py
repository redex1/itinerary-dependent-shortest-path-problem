from application.business_logic.algorithm.utility.time import Time
from application.business_logic.algorithm.utility.constants import FIRST_ACTION


class Node:
    def __init__(self, state, action=FIRST_ACTION, parent=None, transport_cost=Time.to_time("00:00")):
        self.state = state
        self.action = action
        self.parent = parent
        self.transport_cost = transport_cost

    def child_node(self, problem, action):
        new_state = problem.get_new_state(self.state, action)
        new_costs = problem.path_cost(self.transport_cost, self.state, action)
        return Node(new_state, action, self,
                    new_costs["new_transport_cost"])

    def solution(self):
        return [(node.state, node.action) for node in self.get_final_path()[:]]

    def get_final_path(self):
        node, path_back = self, []
        while node:
            path_back.append(node)
            node = node.parent
        return list(reversed(path_back))

    def __lt__(self, other):
        return self.transport_cost < other.transport_cost
