class Branch:
    def __init__(self, friendly_id, capacity, quantity, is_active, continent, latitude, longitude):
        self.friendly_id = friendly_id
        self.capacity = capacity
        self.quantity = quantity
        self.is_active = is_active
        self.continent = continent
        self.latitude = latitude
        self.longitude = longitude

    def is_active_branch(self):
        return self.is_active == 1
