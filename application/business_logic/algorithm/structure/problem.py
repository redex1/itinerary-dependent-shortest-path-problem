from application.business_logic.algorithm.utility.time import Time
from application.business_logic.algorithm.utility.heuristic import calculate_distance_time_to_goal


class Problem:
    def __init__(self, initial, goal, network, branchs):
        self.initial = initial
        self.goal = goal
        self.network = network
        self.branchs = branchs

    def path_cost(self, transport_cost, orig_state, action):
        new_costs = {}
        destinations = self.network.neighbors[orig_state]

        for destination in destinations:
            if destination[0] == action:
                new_costs["new_transport_cost"] = transport_cost.no_limit_add(Time.to_time(destination[4]))
                break

        return new_costs

    def get_new_state(self, prev_state, action):
        destinations = self.network.neighbors[prev_state]
        for destination in destinations:
            if destination[0] == action:
                return destination[1]
        return "not_found"

    def goal_test(self, state):
        return self.goal == state

    def actions(self, state):
        actions = []
        neighbors = self.network.neighbors[state]
        for neighbor in neighbors:
            actions.append(neighbor[0])
        return actions

    def heuristic(self, state_from):
        return calculate_distance_time_to_goal(self.branchs[state_from].latitude, self.branchs[state_from].longitude,
                                               self.branchs[self.goal].latitude, self.branchs[self.goal].longitude)
