from application.numerical_experimentation.experiment.batch_testing import batch_testing
from application.business_logic.algorithm.utility.constants import A_STAR_ALGORITHM, UNIFORM_COST_ALGORITHM, \
    VORACIOUS_ALGORITHM
from application.numerical_experimentation.experiment.utility.file_extractor import get_files_names

if __name__ == '__main__':
    batch_testing("application/numerical_experimentation/experiment/request_simulation/100_request_batch.txt",
                  "application/numerical_experimentation/api_simulation/100_blocked_flights.txt",
                  "application/numerical_experimentation/database_simulation/branchs_10_percent_off.txt",
                  UNIFORM_COST_ALGORITHM)
    batch_testing("application/numerical_experimentation/experiment/request_simulation/100_request_batch.txt",
                  "application/numerical_experimentation/api_simulation/100_blocked_flights.txt",
                  "application/numerical_experimentation/database_simulation/branchs_10_percent_off.txt",
                  A_STAR_ALGORITHM)
